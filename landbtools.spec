%define py3_support 1
# Compatibility with RHEL. These macros have been added to EPEL but
# not yet to RHEL proper.
# https://bugzilla.redhat.com/show_bug.cgi?id=1307190
%{!?__python3: %global __python3 /usr/bin/python3}
%{!?python3_sitelib: %global python3_sitelib %(%{__python3} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:      landbtools
Version:   24.4.4
Release:   2%{?dist}
Provides:  python%{python3_version}dist(pylandb) = %{version}-%{release}
Summary:   Simplifies Python SOAP calls to CERN LanDB service
Vendor:    CERN
License:   MIT
Group:     System Environment/Base
URL:       https://gitlab.cern.ch/hw/pylandbtools
Source:    %{name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  perl-podlators
Conflicts:      CERN-CC-landbtools
%define __pathfix /usr/bin/pathfix.py -pni

Requires: python(abi) >= 3.6
%if 0%{?el7}
Requires: python36-suds
Requires: python36-pycurl
%else
%if 0%{?el8}
Requires: python%{python3_version}dist(suds-jurko)
Requires: python%{python3_version}dist(pycurl)
%else
%if 0%{?el9}
Requires: python%{python3_version}dist(suds)
Requires: python%{python3_version}dist(pycurl)
%endif
%endif


BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

# If Python3 is available, redefine CLI shebang
%define __python /usr/bin/python3
%define __python_shebangs_opts %{py3_shbang_opts}
%endif


%description
Simplifies Python SOAP calls to CERN LanDB service,
provides a simple type factory and client service to
build generic queries to the WSDL-defined API.
Supports Kerberos5 environment for passwordless auth.
Provides landbGetInfo command for quick queries.

%prep
%setup -q

%build
pod2man data/man/landbGetInfo.pod | gzip > data/man/landbGetInfo.1.gz

CFLAGS="%{optflags}" %{__python3} setup.py build

%install
%{__rm} -rf %{buildroot}

%{__python3} setup.py install --skip-build --root %{buildroot}

%{__mkdir} -p %{buildroot}/%{_mandir}/man1

%{__install} -m 644 data/man/landbGetInfo.1.gz %{buildroot}/%{_mandir}/man1/

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/landbGetInfo.1.gz
%{python3_sitelib}/*

%doc AUTHORS COPYING

%changelog
* Wed Apr 24 2024 - Pawel Zembrzuski <pawel.zembrzuski@cern.ch> - 24.4.4-2
- Add proper package name in "Provices" secrion of the RPM

* Mon Apr 22 2024 - Pawel Zembrzuski <pawel.zembrzuski@cern.ch> - 24.4.4-1
- From this point Python 2 is no more supported
- Do not overwrite valid cookie
- Store token also when it's generated from cookie
- Allow to specify username for LanDB
- Refresh cookie if cookie is present but token can't be fetched

* Tue Nov 14 2023 - Nikos Papakyprianou <nikos.papakyprianou@cern.ch> - 23.11.3-1
- Add password retry mechanism

* Tue Sep 19 2023 - Nikos Papakyprianou <nikos.papakyprianou@cern.ch> - 23.9.3-1
- Include hostname in token path

* Tue Sep 5 2023 - Nikos Papakyprianou <nikos.papakyprianou@cern.ch> - 23.9.1-2
- Try to load available token

* Tue Sep 5 2023 - Nikos Papakyprianou <nikos.papakyprianou@cern.ch> - 23.9.1-1
- Enable user password authentication due to OTG0077767

* Thu Jun 15 2023 - Anthony Grossir <anthony.grossir@cern.ch> - 23.6.3-1
- integrate new sso: replace cern-get-sso-cookie` command line by new `auth-get-sso-cookie`

* Mon May 15 2023 - Anthony Grossir <anthony.grossir@cern.ch> - 23.5.3-1
- remove failing pod2man options in spec file build

* Mon Sep 12 2022 - Steve Traylen <steve.traylen@cern.ch> - 22.9.3-1
- Use macros for python3 version

* Fri Dec 3 2021 - Luca Gardi <luca.gardi@cern.ch> - 21.12.1-1
- deploy on CentOS 9 Stream

* Mon Apr 26 2021 <luca.gardi@cern.ch> - 21.4.5-1
- deploy on CentOS 8 Stream

* Wed Apr 14 2021 <luca.gardi@cern.ch> - 21.4.3-1
- Fixed a typo causing an exception in BytesIO declaration (Py3 only)

* Thu Dec 10 2020 <luca.gardi@cern.ch> - 20.12.2-1
- added c8s support
- fixed bug in py3 io import
- install for py2 and py3 on CC7

* Thu Dec 10 2020 <luca.gardi@cern.ch> - 20.12.2-1
- Fixed serial number attribute visibility
- minor linting

* Tue Nov 14 2017 <afiorot@cern.ch> - 2.0.1-1
- Fixed serial number attribute visibility
- minor linting

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-4
- Fixed issue in custom cookie-path setter
- Wrote a proper README file for pylandb

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-3
- this changelog is now correct

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-2
- Now generated with correct dist in rpm

* Mon Nov 13 2017 <luca.gardi@cern.ch> - 2.0.0-1
- Rewrote everything in python
- Provides pylandb library

* Fri Jul 31 2015 <tfabio@cern.ch> - 1.2.7-2
- The OS name accepts spaces (Windows 2012 R2)

* Thu Jul 30 2015 <tfabio@cern.ch> - 1.2.7-1
- Added possibility to update Landb Operating System (name)

* Thu Jul 30 2015 <tfabio@cern.ch> - 1.2.6-1
- Added possibility to update Landb TAG and DESCRIPTION fields

* Wed Dec 10 2014 <aurelien@cern.ch> - 1.2.5-1
- Landb token support + other stuffs

* Wed Feb 26 2014 <aurelien@cern.ch> - 1.2.4-1
- LanDB token support

* Mon Jan 27 2014 <aurelien@cern.ch> - 1.2.3-2
- Hypervisor and VM checking

* Thu Feb 28 2013 <lefebure@cern.ch> - 1.2.3-1
- landbAddIPMI enhanced by Elisiano's patch (more checking)

* Wed Feb 13 2013 <uschwick@cern.ch> - 1.2.2-2
- resolve conflicts

* Tue Feb 12 2013 <lefebure@cern.ch> - 1.2.2-1
- Add landbAddIPMI

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.2.1-1
- bug fix for -T flag

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.2.0-1
- koji patches
- redo passwd treatment
- rename from CERN-CC-landbtools to landbtools

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.1.5-1
- add landbRenameDevice
- koji patches
- rename from CERN-CC-landbtools to landbtools

* Tue Oct 11 2011 <uschwick@cern.ch> - 1.1.3-1
- allow blanks in cluster names

* Tue Aug 23 2011 <uschwick@cern.ch> - 1.1.2-1
- import updates to landbGetInfo from Elisiano
- jenkins and SLC6 build patches

* Wed May 18 2011 <uschwick@cern.ch> - 1.1.1-2
- add landbGetClusterInfo and landbMoveToPrimaryService

* Wed Nov 24 2010 <uschwick@cern.ch> - 1.1.0-2
- change logic in landbBindMAC to allow binding macs of
  interfaces on secondary services

* Fri Sep 17 2010 <uschwick@cern.ch> - 1.1.0-1
- change naming convention a bit (2 digits instead of 3)

* Fri Sep 10 2010 <uschwick@cern.ch> - 1.0.9-1
- fixings for 10GE machines upstairs

* Wed Sep 08 2010 <uschwick@cern.ch> - 1.0.8-1
- guess building from switch name instead of hardcoding it

* Wed Jun 23 2010 <uschwick@cern.ch> - 1.0.7-1
- add landbUpdateDevice

* Thu Jun 17 2010 <uschwick@cern.ch> - 1.0.6-1
- landbBindMAC now overwrites wrong bindings
- rename -G interface in landbMoveToSecondaryService
  following the naming conventions

* Tue Jun 1 2010 <uschwick@cern.ch> - 1.0.5-1
- add possibility to specify the first index of a VM in
  landbRegisterVM minor bug fix in landbGetInfo

* Tue Jun 1 2010 <uschwick@cern.ch> - 1.0.4-1
- add landbUnconfigure10GE
- reshuffle module handling
- add landbBindMAC

* Mon May 31 2010 <uschwick@cern.ch> - 1.0.2-1
- add landbRenameInterface
- make landbConfigure10GE more fail safe

* Mon May 10 2010 <uschwick@cern.ch> - 1.0.1-1
- make naming more consistent

* Mon May 10 2010 <uschwick@cern.ch> - 1.0.0-1
- repackage
- add tools for

* Thu Apr 22 2010 <uschwick@cern.ch> - 0.0.7-1
- add landbAddVM

* Fri Mar 26 2010 <uschwick@cern.ch> - 0.0.6-1
- increase max number of leases per hypervisor

* Thu Jan 28 2010 <uschwick@cern.ch> - 0.0.5-1
- add hypervisor name in the output of landbQueryVMs

* Mon Nov 16 2009 <uschwick@cern.ch> - 0.0.4-1
- bug fix ...

* Mon Nov 16 2009 <uschwick@cern.ch> - 0.0.3-1
- add landbGetInfo
- add landbMigrateVM

* Fri Nov 13 2009 <uschwick@cern.ch> - 0.0.2-1
- implement verbose mode for landbQuery

* Wed Nov 11 2009 <uschwick@cern.ch> - 0.0.1-2
- improved passwd passing in vmConfig.pm

* Mon Nov 9 2009 <uschwick@cern.ch> - 0.0.1-1
- Initial build.

from __future__ import print_function
import argparse
import sys
from pylandb import LanDB


def get_arguments():
    """ Configure ArgumentParser and retrieve the input arguments """
    # Optional arguments
    args = argparse.ArgumentParser('LanDB queries tool')
    args.add_argument('-d', '--debug', action='store_true',
                      help='Show debug information')
    args.add_argument('-n', '--network', action='store_true',
                      help='Show the network information only '
                           '(type and mac address)')
    args.add_argument('-i', '--ip_services', action='store_true',
                      help='Show the ip service information')
    args.add_argument('-a', '--ip_aliases', action='store_true',
                      help='Show the ip aliases information')
    args.add_argument('-v', '--vms', action='store_true',
                      help='Shows the child VMs or parent hypervisor')
    args.add_argument("-u", "--user", action="store",
                      help="Specify username used for authentication. "
                           "By default currently logged in user will be used")
    # Positional arguments
    args.add_argument('devices', nargs='*', help='device(s)')
    return args.parse_args()


def landb_get_info():
    """ Emulated legacy output from PERL script landbGetInfo """
    # Set up arguments
    arguments = get_arguments()

    if not arguments.devices:
        sys.exit('No devices specified, nothing to do.')
    # Setting up LanDB service
    landb = LanDB(username=arguments.user, debug=arguments.debug)
    landb_service = landb.get_service()

    for device in arguments.devices:
        dev = landb_service.getDeviceInfo(device)
        # This is the legacy output format
        if not arguments.vms:
            print("%-20s = %s" % ('DeviceName', dev['DeviceName']))

        if arguments.network:
            # Requesting network info
            if dev['NetworkInterfaceCards']:
                for ncard in dev['NetworkInterfaceCards']:
                    print("%-20s = %-20s %s" % ('NetworkInterfaceCards',
                                                ncard['CardType'],
                                                ncard['HardwareAddress']))
        elif arguments.ip_services:
            # Requesting IP services info
            if dev['Interfaces']:
                for iface in dev['Interfaces']:
                    sname = iface['ServiceName']
                    ip_servs = landb_service.getSwitchesFromService(sname)
                    print("%-20s = %s (%s)" % ('ServiceName', sname,
                                               ', '.join(ip_servs)))
        elif arguments.ip_aliases:
            # Requesting IP aliases info
            aliases = []
            if dev['Interfaces']:
                for iface in dev['Interfaces']:
                    if '-IPMI' in iface['Name'].upper():
                        continue
                    if not iface['IPAliases']:
                        continue
                    for alias in iface['IPAliases']:
                        alias = alias.upper()
                        aliases.append(alias.replace('.CERN.CH', ''))
            if not aliases:
                aliases = ['None']
            print("%-20s = %s" % ("%s_ALIASES" % dev['DeviceName'],
                                      " ".join(aliases)))
        elif arguments.vms:
            # Requesting VMs info
            vm_info = landb_service.vmGetInfo(dev['DeviceName'])
            if vm_info['IsVM']:
                # Device is a VM
                print("%-20s = %s (VM)" % ('DeviceName', dev['DeviceName']))
                print("%-20s = %s" % ('VMParent', vm_info['VMParent']))

            else:
                if vm_info['VMGuestList']:
                    # Device is an hypervisor
                    print("%-20s = %s (Hypervisor)" \
                          % ('DeviceName', dev['DeviceName']))
                    print("%-20s = %s" \
                          % ('VMGuestList', ','.join(vm_info['VMGuestList'])))
                else:
                    # Device is a host
                    print("%-20s = %s (Host)" \
                          % ('DeviceName', dev['DeviceName']))

        else:
            # Requesting generic info
            print("%-20s = %-4s-%-s-%-4s" \
                  % ('Location',
                     dev['Location']['Building'],
                     dev['Location']['Floor'],
                     dev['Location']['Room']))
            print("%-20s = %s" % ('Zone', dev['Zone']))
            if dev['SerialNumber']:
                print("%-20s = %s" % ('SerialNumber', dev['SerialNumber']))
            print("%-20s = %s" % ('Manufacturer', dev['Manufacturer']))
            print("%-20s = %s" % ('Status', dev['Status']))
            print("%-20s = %s" % ('Model', dev['Model']))
            print("%-20s = %s" % ('GenericType', dev['GenericType']))
            print("%-20s = %s" % ('Tag', dev['Tag']))
            if dev['ResponsiblePerson']:
                print("%-20s = %-15s %-15s %-2s-%-3s" \
                      % ('ResponsiblePerson',
                         dev['ResponsiblePerson']['Name'],
                         dev['ResponsiblePerson']['FirstName'],
                         dev['ResponsiblePerson']['Department'],
                         dev['ResponsiblePerson']['Group']))
            if dev['UserPerson']:
                print("%-20s = %-15s %-15s %-2s-%-3s" \
                      % ('UserPerson',
                         dev['UserPerson']['Name'],
                         dev['UserPerson']['FirstName'],
                         dev['UserPerson']['Department'],
                         dev['UserPerson']['Group']))
            if dev['Interfaces']:
                ifacec = 0
                for ifc in dev['Interfaces']:
                    # For every interface show IP and,
                    # if possible, bound MAC address
                    ifc_name = ifc.Name.upper().replace('.CERN.CH', '')
                    print("%-20s = %s" \
                          % ("%s_IP%s" % (ifc_name, ifacec),
                             ifc['IPAddress']))
                    if ifc['BoundInterfaceCard']:
                        # Attribute BoundInterfaceCard might not be set
                        print("%-20s = %s" \
                              % ("%s_MAC%s"
                                 % (ifc_name, ifacec),
                                 ifc['BoundInterfaceCard']['HardwareAddress']))
                    ifacec += 1


def main():
    landb_get_info()


if __name__ == '__main__':
    main()

import json
import logging
import os
import time
import subprocess
import getpass
import socket
from datetime import datetime, timedelta
from http.cookiejar import MozillaCookieJar, LoadError
from json import JSONDecodeError
from pathlib import Path

import pycurl
from suds import WebFault
from suds.client import Client
from suds.sax.element import Element
from suds.xsd.doctor import Import, ImportDoctor

try:
    from StringIO import StringIO as IOHandler
except ImportError:
    from io import BytesIO as IOHandler


class LanDB(object):
    """
        Simplifies SOAP calls to CERN LanDB service, provides a simple
        type factory and client service to build generic queries to the
        WSDL-defined API.

        Usage example:
            landb = LanDB()
            landb_service = landb.get_service()
            print landb_service.getDeviceInfo('VALIDHOSTNAME')
    """

    class GetSSOCookieError(Exception):
        """ Unable to retrieve cookie against CERN Single Sign-on service """
        pass

    class GetLanDBTokenError(Exception):
        """Raise when there is a failure retrieving a LanDB token using the SSO cookie."""
        pass

    TOKEN_URL        = 'https://network.cern.ch/sc/soap/soap.fcgi/sso/6/token'
    API_URL          = 'https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL'
    API_SCHEMA_URL   = 'http://schemas.xmlsoap.org/soap/encoding/'
    LOGGER_NAME      = 'landb_handler'
    FOLDER_NAME      = '.pylandb'
    COOKIE_NAME      = 'webreq-sso-cookie.dat'
    TOKEN_NAME       = 'landb_token_%s' %socket.gethostname()
    CERN_AFS_HOME    = '/afs/cern.ch/user/'
    CERN_AFS_PRIVATE = 'private'
    cookie_path      = None
    token            = None
    logging          = None

    def __init__(self, debug=False, cookie_path=None, username=None):
        """ Initializes logging, authentication and SOAP client """

        if debug:
            loglevel = logging.DEBUG
        else:
            loglevel = logging.ERROR

        self.username = username

        self._set_logger(loglevel)
        self._set_cookie_path(cookie_path)
        self._set_initial_client()

        # Try to get a LanDB token from cookie or password
        try:
            if not self._valid_token():
                self._set_token_from_cookie()
        except (self.GetLanDBTokenError, self.GetSSOCookieError):
            self._set_token_from_password()

        self._set_client()
        self.logger.info('LanDB handler initialized')

    def _save_token(self):
        """Save token"""
        self.logger.info('Save token in %s' % self.token_path)

        with open(self.token_path, 'w+', encoding='utf-8') as file_handler:
            os.chmod(self.token_path, 0o600)
            json.dump(self.token, file_handler)

    def _get_cookie_file(self):
        cookie_file = Path(self.cookie_path)
        if not cookie_file.exists() or cookie_file.stat().st_size == 0:
            return None

        return cookie_file

    def _valid_cookie(self):
        """Checks the validity of the cookie file"""
        cookie_file = self._get_cookie_file()
        if not cookie_file:
            return False

        cookie = MozillaCookieJar(str(cookie_file))
        try:
            cookie.load()
        except LoadError as exc:
            self.logger.debug("Can't load Cookie file.")
            self.logger.debug("%s", exc)
            return False

        for entry in cookie:
            # If there is less than 7 hours till cookie expiration
            # (there is an issue with wrong expiration date in the cookie)
            if entry.expires and datetime.fromtimestamp(entry.expires) < (datetime.now() - timedelta(hours=7)):
                return False

        return True

    def _valid_token(self):
        """Checks if LanDB token is valid"""
        self.logger.info('Try to load local LanDB token')
        token_file = Path(self.token_path)

        # Check if token exists
        if not token_file.exists():
            return False

        token_file_stats = token_file.stat()
        max_time_difference = timedelta(hours=9, minutes=30)
        creation_time = datetime.fromtimestamp(token_file_stats.st_ctime)

        # Check if the token is not older than 9.5 hours
        if (datetime.now() - creation_time) > max_time_difference:
            return False

        # Load saved token
        try:
            self.token = json.load(token_file.open())
        except JSONDecodeError:
            return False

        if self.token is None:
            return False

        return True

    def _set_cookie_path(self, cookie_path):
        """ Checks and creates folder for temporary and safe token storage """
        self.logger.info('Setting cookie path')
        if not cookie_path:
            # Default cookie path
            home_path = os.path.expanduser('~/')
            if home_path.startswith(self.CERN_AFS_HOME):
                # Check if script is running in CERN AFS, in which case
                # use the ~/private folder to save the token file
                cookie_folder = os.path.join(home_path,
                                             self.CERN_AFS_PRIVATE,
                                             self.FOLDER_NAME)
            else:
                # Use user home
                cookie_folder = os.path.join(home_path, self.FOLDER_NAME)

            self.cookie_path = os.path.join(cookie_folder, self.COOKIE_NAME)
            # Set also the token path
            self.token_path = os.path.join(cookie_folder, self.TOKEN_NAME)
        else:
            # User specified folder
            self.cookie_path = os.path.join(cookie_path)

        # Check if folder exists
        if not os.path.exists(os.path.dirname(self.cookie_path)):
            self.logger.info("Creating folder %s"
                             % os.path.dirname(self.cookie_path))
            os.makedirs(os.path.dirname(self.cookie_path))

        self.logger.info('Cookie path set to %s' % self.cookie_path)

    def _set_logger(self, loglevel=None):
        """ Sets a logger and suppresses verbose logs from suds library """
        # Configuring logging options
        logging.getLogger('suds.client').setLevel(logging.ERROR)
        logging.getLogger('suds.transport').setLevel(logging.ERROR)
        logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
        logging.getLogger('suds.transport.http').setLevel(logging.ERROR)

        self.logger = logging.getLogger(self.LOGGER_NAME)
        self.logger.setLevel(loglevel)

        if not self.logger.handlers:
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.INFO)

            formatter = logging.Formatter(
                '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            console_handler.setFormatter(formatter)

            self.logger.addHandler(console_handler)
            self.logger.info('Logger initialized')

    def _set_cookie(self):
        """ Retrieves and writes authentication cookies """
        self.logger.info('Setting cookie')
        # Checking if path is writable
        try:
            with open(self.cookie_path, 'w+'):
                # Getting the cookie through auth-get-sso-cookie,
                # saved in ~/private/
                cmd_str = 'auth-get-sso-cookie -u "%s" '\
                          '-o "%s"' % (self.TOKEN_URL, self.cookie_path)
                cmd = subprocess.Popen(cmd_str, shell=True,
                                       executable='/bin/bash',
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
                cmd_rpl, dummy = cmd.communicate()
                if cmd_rpl:
                    raise self.GetSSOCookieError('Unexpected output for: %s'
                                                 % cmd_str)
        except IOError as e:
            raise self.GetSSOCookieError('Unable to write cookie file in %s: %s '
                                         % (self.cookie_path, e.msg))
        if self._valid_cookie() is False:
            raise self.GetSSOCookieError("Can't request cookie for this user")

    def _set_token_from_cookie(self, retry=False):
        """ Retrieves and sets authentication token, against LanDB SOAP
            APIs, using a stored cookie
        """
        self.logger.info('Setting token from cookie')
        if not self._valid_cookie():
            self._set_cookie()

        output = IOHandler()
        curl_handler = pycurl.Curl()
        curl_handler.setopt(curl_handler.URL, str(self.TOKEN_URL))
        curl_handler.setopt(curl_handler.WRITEFUNCTION, output.write)
        curl_handler.setopt(curl_handler.FOLLOWLOCATION, True)
        curl_handler.setopt(curl_handler.COOKIEFILE, self.cookie_path)
        curl_handler.perform()
        curl_handler.close()
        try:
            response = json.loads(output.getvalue())
        except json.decoder.JSONDecodeError:
            if retry is False:
                self._set_cookie()
                self._set_token_from_cookie(retry=True)
                return
            raise self.GetLanDBTokenError
        self.token = response['token']
        self._save_token()

    def _set_token_from_password(self):
        """ Retrieves and sets authentication token, against LanDB SOAP
            APIs, using a username and password
        """
        self.logger.info('Setting token from password')
        username = self.username or getpass.getuser()

        # Run until a correct password has been provided
        while True:
            try:
                password = getpass.getpass(prompt='LanDB access requires your password: ', stream=None)
                self.token = self.client.service.getAuthToken(username, password, 'CERN')
                break
            except WebFault:
                print('Wrong password! Please try again.')
            except Exception as e:
                print('An unexpected error occurred: %s' % e)
                break  

        self._save_token()

    def _set_initial_client(self):
        """ Imports SOAP schema and sets the call client """
        self.logger.info('Setting initial client')
        # Creating the client
        schema = Import(self.API_SCHEMA_URL)
        doctor = ImportDoctor(schema)
        client = Client(self.API_URL, doctor=doctor, cache=None)
        self.client = client

    def _set_client(self):
        """ Imports SOAP schema and sets the call client """
        self.logger.info('Setting client based on acquired token')
        
        # Authentication
        auth_token_element = Element('token').setText(self.token)
        auth_header_element = Element('Auth').insert(auth_token_element)
        self.client.set_options(soapheaders=auth_header_element)


    def get_factory(self, types):
        """ Returns a types factory for LanDB WSDL definition """
        return self.client.factory.create(types)

    def get_service(self):
        """ Returns the SOAP client service for WSDL defined function calls """
        return self.client.service

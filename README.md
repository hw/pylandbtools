pylandb
==========

Features
--------
* **KRB5 authentication**

  `pylandb` provides Kerberos-only access to LanDB, no password
  prompts are needed anymore

* **Easy cookie file management**

  `pylandb` supports custom path for safe session-persistent
  cookie file storage.

  Example:
  * default:

    ```python
    In [1]: from pylandb import LanDB
    In [2]: landb_handler = LanDB()
    In [3]: landb_handler.cookie_path
    Out[3]: '/root/.pylandb/webreq-sso-cookie.dat'
    ```

  * default under `afs`:

    ```python
    In [3]: landb_handler.cookie_path
    Out[3]: '/afs/cern.ch/user/l/lgardi/private/.pylandb/webreq-sso-cookie.dat'
    ```

  * custom:

    ```python
    In [2]: landb_handler = LanDB('/home/lgardi/.pylandb_cookie')
    In [3]: landb_handler.cookie_path
    Out[3]: ''/home/lgardi/.pylandb_cookie''
    ```

* **Singleton ready and thread-safe**

  To avoid the overhead of the authentication handshake, it's possible to feed
  the LanDB object to multiple threads.

  ```python
  landb = LanDB(debug=arguments.debug)
  threads = [MyThread(landb=landb) for i in range(5)]
  ```

* **Legacy CLI tool**

  This package provides legacy support for `landbGetInfo`,
  see `man landbGetInfo` for detailed usage.

Usage
-----
* **Simple calls**

    Just look up the [NetworkService V6 Description](https://network.cern.ch/sc/soap/6/description.html)
    to find the right operation for you and use the service getter to call it.

    Example:
    ```
    - getDeviceBasicInfo
      * Input: device name (xsd:string)
      * Output: device information (types:DeviceBasicInfo)
      * Authentication: none
      * Returns the basic information of the device given as parameter
    ```

    ```python
    In [1]: from pylandb import LanDB
    In [2]: landb_handler = LanDB()
    In [3]: landb_service = landb_handler.get_service()
    In [4]: landb_service.getDeviceBasicInfo('lgardi-mysql')
    Out[4]:
    (DeviceInfo){
       DeviceName = "LGARDI-MYSQL"
       Zone = "0000"
       Status = "ACTIVE"
       Manufacturer = "KVM"
       Model = "VIRTUAL MACHINE"
       GenericType = "COMPUTER"
       Description = None
       Tag = "OPENSTACK VM"
       SerialNumber = None
       OperatingSystem =
          (OperatingSystem){
             Name = "LINUX"
             Version = "UNKNOWN"
          }
       ...
     }
    ```
* **Using the type factory**

  Use the built-in type factory to forge the input parameters.

  Example:
  ```
  - getHCPInfoArray
    * Input: list of devices (types:ArrayOfString)
    * Output: interface and hardware address list (types:ArrayOfInetInfo)
    * Authentication: CERN privileged account
    * Gets IP interface information (including hardware address) from a list of devices.
  ```

  ```python
  In [1]: from pylandb import LanDB
  In [2]: landb_handler = LanDB()
  In [3]: landb_service = landb_handler.get_service()
  In [4]: input_array = landb_handler.get_factory('types:ArrayOfString')
  In [5]: input_array.items = ['lgardi-mysql', 'WINZOZ8-GRDLCU']
  In [6]: landb_service.getHCPInfoArray(input_array)
  Out[6]:
  [(InetInfo){
      HostName = "LGARDI-MYSQL"
      HardwareAddress = "02-16-3E-01-B3-DF"
      IP = "188.185.77.130"
      NetAddress = "188.185.76.0"
      Mask = "255.255.252.0"
      GatewayAddress = "188.185.76.1"
    }, (InetInfo){
      HostName = "WINZOZ8-GRDLCU"
      HardwareAddress = "02-16-3E-01-AD-CF"
      IP = "188.185.117.216"
      NetAddress = "188.185.116.0"
      Mask = "255.255.252.0"
      GatewayAddress = "188.185.116.1"
    }]

  ```

Build RPMs
----------

Using Koji: `koji build ai7 --nowait git+ssh://git@gitlab.cern.ch:7999/CC-SysAdmins/pylandbtools.git#<TAG>`

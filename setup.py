#!/usr/bin/env python
from setuptools import setup, find_packages
setup(
    name='landbtools',
    version='24.4.4-2',
    description="Simplifies Python SOAP calls to CERN LanDB service",
    author="Luca Gardi, Nikos Papakyprianou, Pawel Zembrzuski",
    long_description="Simplifies Python SOAP calls to CERN LanDB service, "
                     "provides a simple type factory and client service to "
                     "build generic queries to the WSDL-defined API."
                     "Supports Kerberos5 environment for passwordless auth."
                     "\n \n"
                     "Provides landbGetInfo command for quick queries.",
    author_email="nikos.papakyprianou@cern.ch",
    url="https://gitlab.cern.ch/hw/pylandbtools",
    maintainer='CERN IT-FA-HCT',
    maintainer_email='it-dep-fa-hct@cern.ch',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        "console_scripts": [
            "landbGetInfo=pylandb.bin.landbGetInfo:main",
        ]
    },
)
